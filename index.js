/* jshint node: true */
'use strict';

module.exports = {
  name: 'ember-cli-simple-statistics',

  included: function(app) {
   this._super.included(app);
   this.app.import(app.bowerDirectory + '/simple-statistics/src/simple_statistics.js');
  }
};
